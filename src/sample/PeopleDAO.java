package sample;

public interface PeopleDAO {
    String PEOPLE_TABLE = "people";
    String FIRSTNAME = "people_firstname";
    String LASTNAME = "people_lastname";
    String PASSWORD = "password";
    String LOGIN = "login";
}
