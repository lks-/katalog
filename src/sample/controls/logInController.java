package sample.controls;


import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.forJodWithDB.DatabaseHandler;
import sample.forJodWithDB.User;
import sample.anim.Shake;

/**
 * Класс контролер окна авторизации
 *
 * @author Lukashevich A.E. 17IT18
 */

public class logInController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button login;

    @FXML
    private TextField log;

    @FXML
    private TextField pass;

    @FXML
    private ImageView back;

    @FXML
    void initialize() {
        back.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/sample.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }


            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            back.getScene().getWindow().hide();
            stage.showAndWait();
        });

        login.setOnAction(event -> {
            String loginText = log.getText().trim();
            String passwordText = pass.getText().trim();

            if(!loginText.equals("") && !passwordText.equals(""))
                loginUser(loginText,passwordText);
             else
                System.out.println("Незаполнены поля");
        });
    }

    /**
     * Метод для авториции пользователя(поиск в бд пользователя с определюнным логином и паролем)
     * @param loginText - логин
     * @param passwordText - пароль
     */
    private void loginUser(String loginText, String passwordText) {
        DatabaseHandler dbHandler = new DatabaseHandler();
        User user = new User();
        user.setLogin(loginText);
        user.setPassword(passwordText);
        ResultSet result = dbHandler.getUser(user);

        int counter = 0;

        while(true){
            try {
                if (!result.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            counter++;
        }

        if(counter >=1){
            System.out.println("Success!");
        }
        else{
            Shake userLogAnim = new Shake(log);
            Shake userPasAnim = new Shake(pass);
            userLogAnim.playAnim();
            userPasAnim.playAnim();
        }
    }
}
