package sample.controls;

import java.io.IOException;
import java.net.URL;

import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.forJodWithDB.DatabaseHandler;
import sample.forJodWithDB.User;

/**
 * Класс контролер окна с регистрацией
 *
 * @author Lukashevich A.E. 17IT18
 */
public class singUpController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button reg;

    @FXML
    private TextField firstname;

    @FXML
    private TextField log;

    @FXML
    private TextField lastname;

    @FXML
    private TextField pass;

    @FXML
    private ImageView back;

    @FXML
    void initialize() {
        back.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/sample.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }


            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            back.getScene().getWindow().hide();
            stage.showAndWait();
        });

        reg.setOnAction(event -> {
            singUpNewUser();
        });
    }


    private void singUpNewUser() {
        DatabaseHandler dbHandler = new DatabaseHandler();

        String nameUser = firstname.getText();
        String lastNameUser = lastname.getText();
        String password = pass.getText();
        String login = log.getText();

        User user = new User(nameUser,lastNameUser,password,login);

        dbHandler.singUpUser(user);
    }
}
