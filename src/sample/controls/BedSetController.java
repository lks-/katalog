package sample.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Класс контролер окна с каталогом кроватей
 *
 * @author Lukashevich A.E. 17IT18
 */
public class BedSetController {

    @FXML
    private ImageView blackBed;

    @FXML
    private ImageView blueBed;

    @FXML
    private ImageView brownBed;

    @FXML
    private ImageView brownBedWidthRed;

    @FXML
    private ImageView darkWoodBed;

    @FXML
    private ImageView back;

    @FXML
    void initialize() {
        //Назад
        back.setOnMouseClicked(event -> back.getScene().getWindow().hide());
        //Информация про blackBed
        blackBed.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Чёрная кровать   ");
                infoWin.setWidth("150/210 см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Дерево окрашенное");
                infoWin.setInfo("Двуспальная кровать");
                infoWin.setPrice("5000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });

        //Инфорамция про blueBed
        blueBed.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Голубая кровать  ");
                infoWin.setWidth("150/210 см");
                infoWin.setHeight("50 см");
                infoWin.setMaterial("Дерево окрашенное ");
                infoWin.setInfo("Двуспальняя кровать");
                infoWin.setPrice("6000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });

        //Информация про brownBed
        brownBed.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Коричневая кровать  ");
                infoWin.setWidth("120/190 см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Дерево окрашенное");
                infoWin.setInfo("Двуспальняя кровать");
                infoWin.setPrice("6000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
        //Информация про brownBedWidthRed
        brownBedWidthRed.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Коричневая кровать ");
                infoWin.setWidth("150/210 см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Дерево окрашенное");
                infoWin.setInfo("Двуспальняя кровать");
                infoWin.setPrice("8000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
        //Информация о darkWoodBed
        darkWoodBed.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Кровать их тёмного дерева ");
                infoWin.setWidth("130/200см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Тёмное дерево");
                infoWin.setInfo("Двуспальняя кровать");
                infoWin.setPrice("10000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
    }
}
