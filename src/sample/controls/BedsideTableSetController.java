package sample.controls;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

/**
 * Класс контролер окна с каталогом тумбочек
 *
 * @author Lukashevich A.E. 17IT18
 */
public class BedsideTableSetController {

    @FXML
    private ImageView blackBedSide;

    @FXML
    private ImageView brownBedSide;

    @FXML
    private ImageView darkBedSide;

    @FXML
    private ImageView modernBedSide;

    @FXML
    private ImageView standartBedSide;

    @FXML
    private ImageView back;

    @FXML
    void initialize() {
    //Назад
    back.setOnMouseClicked(event -> back.getScene().getWindow().hide());
    //Информация про blackBedSide
        blackBedSide.setOnMouseClicked(event -> {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
        try {
            loader.load();
            InfoWin infoWin = loader.getController();
            infoWin.setName("Чёрная тумба   ");
            infoWin.setWidth("60см");
            infoWin.setHeight("80см");
            infoWin.setMaterial("Дерево окрашенное");
            infoWin.setInfo("Три выдвижных полки");
            infoWin.setPrice("1000");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    });

    //Инфорамция про brownBedSide
        brownBedSide.setOnMouseClicked(event -> {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
        try {
            loader.load();
            InfoWin infoWin = loader.getController();
            infoWin.setName("Коричневая тумба ");
            infoWin.setWidth("60см");
            infoWin.setHeight("50см");
            infoWin.setMaterial("Дерево окрашенное ");
            infoWin.setInfo("Три выдвижных полки");
            infoWin.setPrice("800");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    });

    //Информация про darkBedSide
        darkBedSide.setOnMouseClicked(event -> {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
        try {
            loader.load();
            InfoWin infoWin = loader.getController();
            infoWin.setName("Тумба их тёмного дерева  ");
            infoWin.setWidth("50см");
            infoWin.setHeight("70см");
            infoWin.setMaterial("Тёмное дерево");
            infoWin.setInfo("Две выдвижных полки");
            infoWin.setPrice("2000");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    });
    //Информация про modernBedSide
        modernBedSide.setOnMouseClicked(event -> {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
        try {
            loader.load();
            InfoWin infoWin = loader.getController();
            infoWin.setName("Тубма с новым дизайном ");
            infoWin.setWidth("60см");
            infoWin.setHeight("50см");
            infoWin.setMaterial("Дерево окрашенное");
            infoWin.setInfo("Один больной и один маленький отдел");
            infoWin.setPrice("2500");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    });
    //Информация о standartBedSide
        standartBedSide.setOnMouseClicked(event -> {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
        try {
            loader.load();
            InfoWin infoWin = loader.getController();
            infoWin.setName("Стандартная тумба ");
            infoWin.setWidth("80см");
            infoWin.setHeight("50см");
            infoWin.setMaterial("Тёмное дерево");
            infoWin.setInfo("Две выдвижных полки");
            infoWin.setPrice("1500");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    });
}
}
