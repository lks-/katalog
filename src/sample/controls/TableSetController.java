package sample.controls;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;

import javafx.stage.Stage;

/**
 * Класс контролер окна с каталогом столов
 *
 * @author Lukashevich A.E. 17IT18
 */
public class TableSetController {

    @FXML
    private ImageView coffeeTable;

    @FXML
    private ImageView coffeeTableWood;

    @FXML
    private ImageView desk;

    @FXML
    private ImageView oldTable;

    @FXML
    private ImageView pcTable;

    @FXML
    private ImageView back;

    @FXML
    void initialize() {
        //Назад
        back.setOnMouseClicked(event -> back.getScene().getWindow().hide());

        //Информация про pcTable
        pcTable.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Компьютерный стол   ");
                infoWin.setWidth("100см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Дерево тёмное");
                infoWin.setInfo("Стол с подставкой для системного блока,\n" +
                        "выдвижной подставной для клавиатуры\n" + "и ящиками");
                infoWin.setPrice("2000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });

        //Инфорамция про coffeeTable
        coffeeTable.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Кофейный стол   ");
                infoWin.setWidth("50см");
                infoWin.setHeight("20см");
                infoWin.setMaterial("Металл окрашенный");
                infoWin.setInfo("Кофейный столик с необычным дизайном");
                infoWin.setPrice("1500");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
        //Информация про coffeeTableWood

        coffeeTableWood.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Деревянный кофейный стол   ");
                infoWin.setWidth("40см");
                infoWin.setHeight("10см");
                infoWin.setMaterial("Дерево");
                infoWin.setInfo("Маленький кофейный столик");
                infoWin.setPrice("1000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
        //Информация про oldTable
        oldTable.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Антикварный стол   ");
                infoWin.setWidth("90см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Дерево лакированное");
                infoWin.setInfo("Антикрварный стол, сделанный в 1743г");
                infoWin.setPrice("8000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
        //Информация о desk
        desk.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/fxmlFiles/infoWin.fxml"));
            try {
                loader.load();
                InfoWin infoWin = loader.getController();
                infoWin.setName("Письменный стол   ");
                infoWin.setWidth("100см");
                infoWin.setHeight("50см");
                infoWin.setMaterial("Дерево");
                infoWin.setInfo("Письменный стол c ящиками");
                infoWin.setPrice("3000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });
    }
}
