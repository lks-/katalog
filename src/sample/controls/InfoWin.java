package sample.controls;


import java.net.URL;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Класс отображает информацию о выбранном товаре
 * setName - метод для установки информации о имени
 * setWidth - метод для установки информации о ширины
 * setHeight - метод для установки информации о высоты
 * setMaterial - метод для установки информации о материале
 * setInfo - метод для установки информации о дополнительной информации
 * setPrice - метод для установки информации о цене
 *
 * @author Lukashevich A.E. 17IT18
 */
public class InfoWin {

    @FXML
    private URL location;

    @FXML
    private Label name;

    @FXML
    private Label width;

    @FXML
    private Label height;

    @FXML
    private Label material;

    @FXML
    private Label info;
    @FXML
    private Label price;


    public void setName(String name) {
        this.name.setText(name);
    }

    public void setWidth(String width) {
        this.width.setText("Ширина: " + width);
    }

    public void setHeight(String height) {
        this.height.setText("Высота: " + height);
    }

    public void setMaterial(String material) {
        this.material.setText("Материал: " + material);
    }

    public void setInfo(String info) {
        this.info.setText("Информация: " + info);
    }

    public void setPrice(String price) {
        this.price.setText("Цена: " + price);
    }

    @FXML
    void initialize() {
    }
}
