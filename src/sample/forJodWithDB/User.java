package sample.forJodWithDB;

/**
 * Класс для получение информации о пользователях из бд
 *
 * @author Lukashevich A.E. 17IT18
 */
public class User {
    private String firstname;
    private String lastname;
    private String login;
    private String password;

    /**
     * Получение данных о пользователе из бд
     * @param firstname - имя
     * @param lastname - фамилия
     * @param login - логин
     * @param password - пароль
     */
    public User(String firstname, String lastname, String login, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.login = login;
        this.password = password;
    }

    public User() {}

    String getFirstname() {
        return firstname;
    }

    String getLastname() {
        return lastname;
    }

    String getLogin() {
        return login;
    }

    String getPassword() {
        return password;
    }



    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
