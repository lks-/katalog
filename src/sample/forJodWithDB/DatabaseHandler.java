package sample.forJodWithDB;

import sample.PeopleDAO;

import java.sql.*;

/**
 * Класс для работы с базой данных
 *
 * @author Lukashevich A.E. 17IT18
 */
public class DatabaseHandler implements PeopleDAO {
    private Connection conn;

    /**
     * установка подключения к базе данных
     *
     * @return - статус подключения
     */
    private Connection getBdConnection() {
        try {
            String url = "jdbc:postgresql://localhost/postgres";
            String username = "postgres";
            String password = "oragol95";
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url,username,password);
                System.out.println("Connection to Store DB succesfull!");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("Connection failed...");
        }
        return conn;
    }

    /**
     * Регистрация нового пользователя(Запись нового пользователя в таблицу)
     *
     * @param user Пользователь которого нужно записать в базу данных
     */
    public void singUpUser(User user)  {
        String insert = "INSERT INTO " + PEOPLE_TABLE + "(" +
                FIRSTNAME + "," + LASTNAME + "," + PASSWORD +
                "," + LOGIN + ")" + " VALUES(?,?,?,?)";
        try {
            PreparedStatement prSt = getBdConnection().prepareStatement(insert);
            prSt.setString(1, user.getFirstname());
            prSt.setString(2, user.getLastname());
            prSt.setString(3, user.getLogin());
            prSt.setString(4, user.getPassword());


            prSt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Проверка если отпределённый пользователь в таблице(Для авторизации)
     *
     * @param user - Пользователь которого надо найти в базе данных
     * @return
     */
    public ResultSet getUser(User user){
        ResultSet resSet = null;

        String select = "SELECT * FROM " + PEOPLE_TABLE + " WHERE " + LOGIN + "=? AND " + PASSWORD + "=?";

        try {
            PreparedStatement prSt = getBdConnection().prepareStatement(select);
            prSt.setString(1, user.getLogin());
            prSt.setString(2, user.getPassword());

            resSet = prSt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resSet;
    }
}
